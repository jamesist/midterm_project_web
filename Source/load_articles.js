function ajaxCall() {


    $.ajax({
        url: "https://sporadic.nz/2018a_web_assignment_service/Articles",
        type: "GET",

        success: function (msg) {
            console.log(msg);
            // just grab the title and content of the article and display on the page. Still not working.
            var article = $("<div><h4></h4><p></p></div>");
            article.find("h4").text(msg.title);
            article.find("p").text(msg.content);
            $("#main-content").append(article);
        }
    });
}

$(function () {
    $(document).ready(ajaxCall())
});